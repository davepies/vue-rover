const scopeToDirectChildren = selectors =>
  selectors
    .split(",")
    .map(sel => `:scope > ${sel}`)
    .join(",");

// https://www.w3.org/TR/html5/editing.html#focusable
// https://www.w3.org/WAI/PF/aria-practices/#keyboard
export const FOCUSABLE_DESCENDANTS_SELECTOR = `
  a[href]:not([tabindex='-1']),
  area[href]:not([tabindex='-1']),
  input:not([disabled]):not([tabindex='-1']),
  select:not([disabled]):not([tabindex='-1']),
  textarea:not([disabled]):not([tabindex='-1']),
  button:not([disabled]):not([tabindex='-1']),
  iframe:not([tabindex='-1']),
  [tabindex]:not([tabindex='-1']),
  [contentEditable=true]:not([tabindex='-1'])
`;

export const FOCUSABLE_DIRECT_CHILDREN_SELECTOR = scopeToDirectChildren(
  FOCUSABLE_DESCENDANTS_SELECTOR
);

export const DEFAULT_MANUAL_SELECTOR = "[data-focusable]";

export const ARROW_KEY_CODES = {
  left: 37,
  up: 38,
  right: 39,
  down: 40
};

export const SELECTORS = {
  descendants: FOCUSABLE_DESCENDANTS_SELECTOR,
  children: FOCUSABLE_DIRECT_CHILDREN_SELECTOR,
  manual: DEFAULT_MANUAL_SELECTOR,
  default: FOCUSABLE_DESCENDANTS_SELECTOR
};
