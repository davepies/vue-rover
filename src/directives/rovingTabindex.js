import {
  setChecked,
  enableNativeFocus,
  disableNativeFocus,
  arrowNavigation
} from "./utils";
import { SELECTORS } from "./constants";

const setInitialTabIndices = focusableElements => {
  const [firstFocusableElement, ...restOfFocusableElements] = focusableElements;

  // disable tabbing on focusable-elements within the group
  restOfFocusableElements.forEach(disableNativeFocus);
  // make first item focusable, so the group can be tabbed into
  enableNativeFocus(firstFocusableElement);
};

const addArrowNavigation = (el, focusableElements, handleItemSelected) =>
  el.addEventListener(
    "keydown",
    arrowNavigation(focusableElements, handleItemSelected)
  );

const makeItemsFocusableByClick = els =>
  els.forEach(el => el.addEventListener("click", e => setChecked(e.target)));

export default function directive(
  containerEl,
  { arg: chosenSelector, value: handleItemSelected = () => {} }
) {
  const selector = SELECTORS[chosenSelector] ?? SELECTORS.default;
  const focusableElements = [...containerEl.querySelectorAll(selector)];

  if (focusableElements.length === 0) {
    return;
  }

  setInitialTabIndices(focusableElements);

  addArrowNavigation(containerEl, focusableElements, handleItemSelected);

  makeItemsFocusableByClick(focusableElements);
}
