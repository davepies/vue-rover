import { ARROW_KEY_CODES } from "./constants";

export const setChecked = el => {
  el.setAttribute("aria-checked", true);
  el.tabIndex = 0;
  el.focus();
};

export const disableNativeFocus = el => (el.tabIndex = -1);
export const enableNativeFocus = el => (el.tabIndex = 0);

export const getNextElement = els => {
  const currentElementIndex = els.indexOf(document.activeElement);

  return els[currentElementIndex + 1] ?? els[els.length - 1];
};

export const getPreviousElement = els => {
  const currentElementIndex = els.indexOf(document.activeElement);

  return els[currentElementIndex - 1] ?? els[0];
};

export const arrowNavigation = (focusableEls, handleElementSelect) => e => {
  const { keyCode, target } = e;

  switch (keyCode) {
    case ARROW_KEY_CODES.right:
    case ARROW_KEY_CODES.down:
      focusableEls.forEach(disableNativeFocus);
      setChecked(getNextElement(focusableEls));
      break;
    case ARROW_KEY_CODES.left:
    case ARROW_KEY_CODES.up:
      focusableEls.forEach(disableNativeFocus);
      setChecked(getPreviousElement(focusableEls));
      break;
  }

  if (Object.values(ARROW_KEY_CODES).includes(keyCode)) {
    handleElementSelect(target);
    e.preventDefault();
  }
};
