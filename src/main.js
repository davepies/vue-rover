import Vue from "vue";
import App from "./App.vue";

import rovingTabIndex from "./directives/rovingTabindex";

Vue.config.productionTip = false;

Vue.directive("roving-tab-index", rovingTabIndex);

new Vue({
  render: h => h(App)
}).$mount("#app");
